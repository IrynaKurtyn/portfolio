<?php


namespace Tests;


use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthWithDBTest extends TestCase
{
    use RefreshDatabase;
    use DatabaseMigrations;


    public function setUp(): void{

        parent::setUp();
        $this->artisan('passport:install');
    }

}
