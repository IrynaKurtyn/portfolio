<?php

namespace Tests\Feature;

use App\Models\Event;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Tests\AuthWithDBTest;

class CalendarEventTest extends AuthWithDBTest
{

    public function testCalendarIndex(){
        $user = $this->createModel('User');

        for( $i=0; $i< 20; $i ++){
           $this->createModel('Event');
        }


        $this->json('GET', '/api/events')->assertStatus(401);

        $this->actingAs($user, 'api')->json('GET', '/api/events')
            ->assertStatus(200)
            ->assertJsonStructure(['data']); //if pagination - 'links', 'meta'

    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateEmptyEvent()
    {
//        $this->withoutExceptionHandling();

        $this->actingAs($this->createModel('User'), 'api')
            ->json('POST', 'api/events', [])
            ->assertStatus(422);

    }

    public function testCreateValidEvent()
    {
//        $this->withoutExceptionHandling();

        $event = [
            'title' => 'Test',
            'description' => 'Description',
            'priority' => 1,
            'reminder' => true,
            'start' => '2020-05-14T00:00:00+03:00',
            'end' => '2020-05-15T00:00:00+03:00',
            'all_day' => 1
        ];
        $this->json('POST', 'api/events', $event)
            ->assertStatus(401);

       $this->actingAs(factory(User::class)->create(), 'api')
            ->json('POST', 'api/events', $event)
            ->assertJson([ 'data' => [
                'title' => 'Test',
                'description' => 'Description',
                'priority' => 1,
                'reminder' => true,
                'start' => '2020-05-13T21:00:00.000000Z',
                'end' => '2020-05-14T21:00:00.000000Z',
                'all_day' => 1
                ]
            ])
            ->assertJsonStructure(['data' => ['id', 'title', 'description']])
            ->assertStatus(201);
//        Log::info(1, [$response->getContent()]);
        $this->assertDatabaseHas('events', [
            'title' => 'Test',
            'description' => 'Description',
            'priority' => 1,
            'reminder' => true,
            'start' => '2020-05-14 00:00:00',
            'end' => '2020-05-15 00:00:00',
            'all_day' => 1
        ]);

    }


    /**
     *
     */
    public function testFindEvent(){
        $user = $this->createModel('User');
        $event = $this->createModel('Event');
        $this->json('GET', "/api/events/$event->id")->assertStatus(401);
        $this->actingAs($user, 'api')->json('GET', "/api/events/$event->id")->assertStatus(200);
        $this->actingAs($user, 'api')->json('GET', "/api/events/12")->assertStatus(404);
    }

    /**
     *
     */
    public function testUpdateEvent(){
        $user = $this->createModel('User');
        $event = $this->createModel('Event');
        $this->actingAs($user, 'api')->json('PUT', "/api/events/$event->id", [
            'title' => 'Changed',
            'description' => 'Changed',
            'priority' => 1,
            'reminder' => true,
            'start' => '2020-05-14T00:00:00+03:00',
            'end' => '2020-05-15T00:00:00+03:00',
            'all_day' => 1
        ])->assertStatus(200)
            ->assertJsonStructure(['data' => ['backGroundColor']]);
        $this->actingAs($user, 'api')->json('PUT', "/api/events/12")->assertStatus(404);
    }

    /**
     *
     */
    public function testDeleteEvent(){
        $user = $this->createModel('User');
        $event = $this->createModel('Event');
        $this->assertDatabaseHas('events', [
            'id' => $event->id,
        ]);
        $this->actingAs($user, 'api')->json('DELETE', "/api/events/$event->id")->assertStatus(204)
            ->assertSee(null);
        $this->assertDatabaseMissing('events', ['id' => $event->id]);
        $this->actingAs($user, 'api')->json('PUT', "/api/events/-1")->assertStatus(404);
    }


}
