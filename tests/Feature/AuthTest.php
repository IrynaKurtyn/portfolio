<?php

namespace Tests\Unit;

use Faker\Generator;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Laravel\Passport\Passport;
use Laravel\Socialite\Facades\Socialite;
use Mockery\Mock;
use Tests\AuthWithDBTest;
use Mockery;

class AuthTest extends AuthWithDBTest
{

    /**
     *
     */
    public function testCanAuthenticate()
    {
        $this->post('/api/login', [
            'email' => $this->createModel('User')->email,
            'password' => 'password'
        ])->assertStatus(200)
            ->assertJsonStructure(['access_token']);
    }


    /**
     *
     */
    public function testLogout()
    {
//        $this->withoutExceptionHandling();
//        Passport::actingAs(
//            $this->createModel('User')
//        );
        $this->actingAs($this->createModel('User'))
            ->post('/api/logout')->assertStatus(302);

    }

    public function testLoginSocial(){
        $this->withoutExceptionHandling();
        $abstractUser = Mockery::mock('Laravel\Socialite\Two\User');
        $abstractUser->shouldReceive('getId')
            ->andReturn(1234567890)
            ->shouldReceive('getEmail')
            ->andReturn(Str::random(10) . '@gmail.com')
            ->shouldReceive('getNickname')
            ->andReturn('Pseudo')
            ->shouldReceive('getName')
            ->andReturn('Arlette Laguiller')
            ->shouldReceive('getAvatar')
            ->andReturn('https://en.gravatar.com/userimage');

        $provider = Mockery::mock('Laravel\Socialite\Contracts\Provider');
        $provider->shouldReceive('user')->andReturn($abstractUser);

        Socialite::shouldReceive('driver')->andReturn($provider);

    }
}
