<?php


namespace App\Helpers;


use Illuminate\Support\Str;

class XMLPaymentGenerator
{

    private $domTree;

    public function __construct(){
        $this->domTree =  new \DOMDocument('1.0', 'UTF-8');
    }

    /**
     * @return string
     */
    public function initRequest(){
        $domtree = new \DOMDocument('1.0', 'UTF-8');

        $requestTag = $this->domTree->createElement("request");
        $requestTag->setAttribute("version", "1.0");
        $this->domTree->appendChild($requestTag);

        $merchantTag = $requestTag->appendChild($this->domTree->createElement('merchant'));

        $merchantTag->appendChild($this->domTree->createElement('id',env('PRIVAT_MERCHANT_ID')));
        $dataTag = $requestTag->appendChild($this->domTree->createElement('data'));
        $this->getXmlDataContent($this->domTree, $dataTag);

        $signature = $this->generateSignature($dataTag);
        $merchantTag->appendChild($this->domTree->createElement('signature', $signature));
        return $this->domTree->saveXML();
    }


    /**
     * @param \DOMDocument|null $root
     * @param \DOMNode|null $parent
     * @return \DOMDocument
     */
    private function getXmlDataContent(\DOMDocument $root = null, \DOMNode $parent = null): \DOMDocument
    {
        $root = $root ?? new $this->domTree;
        $parent = $parent ?? $root;

        $parent->appendChild($root->createElement('oper','cmt'));
        $parent->appendChild($root->createElement('wait','2'));
        $parent->appendChild($root->createElement('test','1'));


        $paymentTag = $root->createElement('payment');
        $paymentTag->setAttribute('id', Str::random());
        $parent->appendChild($paymentTag);

        $paymentProps = [
            ['name' => 'b_card_or_acc', 'value' => env('PRIVAT_CARD_NUMBER')],
            ['name' => 'amt', 'value' => '1'],
            ['name' => 'ccy', 'value' => 'UAH'],
            ['name' => 'details', 'value' => 'Test'],
        ];

        foreach($paymentProps as $prop){
            $item = $root->createElement("prop");
            foreach($prop as $key => $val){
                $item->setAttribute($key, $val);
            }
            $paymentTag->appendChild($item);
        }

        return $root;
    }


    /**
     * @param $response
     * @return bool
     */
    public function checkSignature($response, $signature){

        $this->domTree->loadXML($response);
        $dataElement = $this->domTree->getElementsByTagName('data')[0] ?? null;
        $calculateSignature = $this->generateSignature($dataElement);
        return $signature == $calculateSignature;
    }


    /**
     * @param $element
     * @return string
     */
    private function generateSignature($element){
        $result = '';
        foreach ($element->childNodes as $child) {
            $result .= $this->domTree->saveXML($child);
        }
        return sha1 (md5($result. env('PRIVAT_MERCHANT_PASSWORD')));
    }

}
