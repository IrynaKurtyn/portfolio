<?php

namespace App\Listeners;


use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use \Laravel\Passport\Events\AccessTokenCreated;
use DB;

class LoginListener
{
    /**
     * Create the event listener.
     *
     * @param  Request  $request
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  AccessTokenCreated  $event
     * @return void
     */
    public function handle(AccessTokenCreated $event)
    {
        DB::table('users')
            ->where('id', $event->userId)
            ->update([
                'last_login_at'    => date('Y-m-d H:i:s'),
            ]);
    }
}
