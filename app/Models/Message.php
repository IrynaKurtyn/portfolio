<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Message extends Model implements Searchable
{
    //

    protected $guarded = [];


    /**
     *
     */
    public function fromUser(){
        return $this->hasOne(User::class, 'id', 'from_id');
    }

    public function getSearchResult(): SearchResult
    {
        $url = route('messages', $this->from_id);

        return new SearchResult(
            $this,
            $this->text,
            $url
        );
    }

}
