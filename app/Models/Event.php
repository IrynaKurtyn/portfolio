<?php

namespace App\Models;

use App\Http\Filters\Filterable;
use App\Http\Resources\Collections\EventCollection;
use App\Http\Resources\Items\EventResource;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Event extends Model implements Searchable
{
    use Filterable;

    const PRIORITY_LOW = 0;
    const PRIORITY_MEDIUM = 1;
    const PRIORITY_HIGH = 2;

    protected $guarded = [];

    protected $dates = [ 'start', 'end'];

    protected $casts = [
        'start' => 'datetime:Y-m-d H:i:s',
        'end' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * @return array
     */
    public static function priorities(){
        return [
            self::PRIORITY_LOW => 'Low',
            self::PRIORITY_MEDIUM => 'Medium',
            self::PRIORITY_HIGH => 'High',
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user(){
        return $this->hasOne(User::class);
    }


    /**
     * @return string
     */
    public function getColor(){
        switch ($this->priority) {
            case 0:
           $color = '#4251f5';
                break;
            case 1:
                $color = '#1a9649';
                break;
            case 2:
                $color = '#b02d05';
                break;
        }

        return $color;
    }


    /**
     * @param $request
     * @return EventCollection|EventResource
     */
    public function createFromRequest(Request $request){

        $this->fill($request->except('periodicy'));
        $this->user_id = auth()->id();
        $periodicy = $request->get('periodicy');
        $start = Carbon::parse($request->get('start'));
        $end = Carbon::parse($request->get('end'));
        $dates = [];
        $dates[] = [
            'start' => $start,
            'end' => $end,
        ];
        if ($periodicy['use']) {
            $interval = $periodicy['every'] . $periodicy['period'];
            $max = Carbon::parse($start)->add('1' . $periodicy['continue'])->subDays(1);
            while ($start < $max){
                $start = Carbon::parse($start)->add($interval);
                $end = Carbon::parse($end)->add($interval);
                $dates[] = [
                    'start' => $start,
                    'end' => $end,
                ];
            }
            $arrayEvents = [];
            foreach ($dates as $date) {
                array_push($arrayEvents, $this->addPeriodicalEvent($date, $request));
            }

            return new EventCollection($arrayEvents);
        } else {
            $this->save();
            return new EventResource($this);
        }
    }


    /**
     * @param $date
     * @param $request
     * @return Event
     */
    private function addPeriodicalEvent($date, $request){
        $periodicalEvent = new Event();
        $periodicalEvent->fill($request->except('periodicy'));
        $periodicalEvent->user_id = auth()->id();
        $periodicalEvent->start = $date['start'];
        $periodicalEvent->end = $date['end'];
        if($periodicalEvent->save()){
            return $periodicalEvent;
        }
    }


    /**
     * @return SearchResult
     */
    public function getSearchResult(): SearchResult
    {
        $url = route('events.index', $this->id);

        return new \Spatie\Searchable\SearchResult(
            $this,
            $this->title,
            $url
        );
    }
}
