<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class Profile extends Model
{
    //

    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * @param $img
     * @return string
     */
    public function saveFile($img)
    {
        $name = 'profile-' . $this->id . '.' . $img->getClientOriginalExtension();
        if (!file_exists(Storage::disk('public')->path('avatars/'))) {
            mkdir(Storage::disk('public')->path('avatars/'));
        }
        $path = Storage::disk('public')->path('avatars/') . $name;
        Image::make($img)->resize(200, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($path);
        return $name;

    }

    /**
     * @return string
     */
    public function getAvatarAttribute($val)
    {
//        dd($val);
        return $val ? Storage::disk('public')->url('/avatars/' . $val) : null;
    }

}
