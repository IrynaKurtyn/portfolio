<?php

namespace App\Providers;

use App\Models\User;
use App\Models\Profile;
use App\Observers\UserObserver;
use App\Observers\ProfileObserver;
use Illuminate\Support\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        User::observe(UserObserver::class);
        Profile::observe(ProfileObserver::class);
    }
}
