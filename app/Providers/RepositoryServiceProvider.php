<?php

namespace App\Providers;

use App\Repositories\EventsRepository;
use App\Repositories\Interfaces\EventsRepositoryInterface;
use App\Repositories\Interfaces\MessagesRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\MessagesRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            UserRepositoryInterface::class,
            UserRepository::class
        );
        $this->app->bind(
            MessagesRepositoryInterface::class,
            MessagesRepository::class
        );
        $this->app->bind(
            EventsRepositoryInterface::class,
            EventsRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
