<?php


namespace App\Http\Resources\Items;


use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->getName(),
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'birth_date' => $this->profile->birth_date,
            'email' => $this->email,
            'phone' => $this->profile->phone,
            'country' => $this->profile->country,
            'city' => $this->profile->city,
            'street' => $this->profile->street,
            'about' => $this->profile->about,
            'avatar_path' => $this->profile->avatar,
        ];
    }
}
