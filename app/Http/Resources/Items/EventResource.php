<?php


namespace App\Http\Resources\Items;


use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'start' => $this->start,
            'end' => $this->end,
            'all_day' => $this->all_day,
            'description' => $this->description,
            'priority' => $this->priority,
            'reminder' => $this->reminder,
            'backGroundColor' => $this->getColor()
        ];
    }
}
