<?php


namespace App\Http\Resources\Collections;


use Illuminate\Http\Resources\Json\ResourceCollection;

class EventCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function($item){
                return [
                    'id' => $item->id,
                    'title' => $item->title,
                    'start' => $item->start,
                    'end' => $item->end,
                    'all_day' => $item->all_day,
                    'reminder' => $item->reminder,
                    'backgroundColor' => $item->getColor()
                ];
            })
        ];
    }
}
