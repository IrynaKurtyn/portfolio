<?php


namespace App\Http\Resources\Collections;


use Illuminate\Http\Resources\Json\ResourceCollection;

class ContactCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function($item){
                return [
                    'id' => $item->id,
                    'name' => $item->getName(),
                    'email' => $item->email,
                    'unread' => $item->unread,
                ];
            })
        ];
    }
}