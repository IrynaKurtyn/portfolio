<?php

namespace App\Http\Resources\Collections;

use App\Models\User;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function($item){
                return [
                    'id' => $item->id,
                    'name' => $item->getName(),
                    'first_name' => $item->first_name,
                    'last_name' => $item->last_name,
                    'email' => $item->email,
                    'address' => $item->profile->street,
                    'city' => $item->profile->city,
                    'country' => $item->profile->country,
                    'phone' => $item->profile->phone,
                    'avatar' => $item->profile->avatar,
                    'birth_date' => $item->profile->birth_date,
                ];
            })
        ];
    }
}
