<?php


namespace App\Http\Resources\Collections;


use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\Items\UserResource;

class MessageCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function($item){
                return [
                    'id' => $item->id,
                    'text' => $item->text,
                    'from' => $item->from_id,
                    'from_user' => new UserResource($item->fromUser),
                    'to' => $item->to_id,
                    'created_at' => $item->created_at,
                ];
            })
        ];
    }
}