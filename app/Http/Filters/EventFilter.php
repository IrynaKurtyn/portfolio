<?php


namespace App\Http\Filters;


use Carbon\Carbon;
use Illuminate\Database\Query\Expression;

class EventFilter extends QueryFilter
{
    public function nextweek(){
        $this->builder->where('start',  '>' , date('Y-m-d H:i:s'))
            ->where('start', '<', Carbon::now()->addWeek());
    }

}
