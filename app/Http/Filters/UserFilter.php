<?php

namespace App\Http\Filters;

use Illuminate\Database\Query\Expression;

class UserFilter extends QueryFilter
{

    /**
     * @param string $search
     */
    public function search(string $search)
    {

       $this->builder->where('email',  'LIKE' , '%' . $search . '%')
            ->orWhere(new Expression("CONCAT(`first_name`, ' ', `last_name`)"), 'like', '%' . $search . '%');
    }

    /**
     * @param string $value
     */
    protected function sort(string $value)
    {
        collect(explode(',', $value))->mapWithKeys(function (string $field) {
            switch (substr($field, 0, 1)) {
                case '-':
                    return [substr($field, 1) => 'desc'];
                case ' ':
                    return [substr($field, 1) => 'asc'];
                default:
                    return [$field => 'asc'];
            }
        })->each(function (string $order, string $field){
            if($field == 'birth_date'){
                return $this->sortBirthDate($order);
            }
            $this->builder->orderBy($field, $order);
        });
    }


    /**
     * @param $order
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function sortBirthDate($order){
        return $this->builder
            ->join('profiles', function ($join) {
                $join->on('users.id', '=', 'profiles.user_id');

            })
            ->orderBy('birth_date', $order);
    }

}

