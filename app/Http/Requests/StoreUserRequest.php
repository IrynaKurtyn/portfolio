<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreUserRequest extends FormRequest
{
    public $id;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
      return  Auth::user()->hasRole('admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $id = $this->route()->parameter('user') ? $this->route()->parameter('user') : '';
        return [
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'email'     => ['required', 'email', 'unique:users,email,' . $id],
            'role' => ['required', 'string', 'exists:roles,name'],
            'send_сredentials' => 'boolean',
            'password' => ['string', 'min:6', 'nullable'],
            'send_credentials' => 'boolean',
            'score_index' => ['numeric', 'nullable'],
            'comment' => ['string', 'nullable'],
            'image' => 'image|mimes:jpeg,png,jpg,pdf,svg|max:2048',
        ];
    }
}
