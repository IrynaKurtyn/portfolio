<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    public $id;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $id = $this->route()->parameter('user') ? $this->route()->parameter('user') : auth()->id();
        return [
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'email'     => ['required', 'email', 'unique:users,email,' . $id],
            'avatar' => ['image', 'mimes:jpeg,png,jpg,pdf,svg', 'max:2048', 'nullable'],
            'about' => ['string', 'nullable'],
            'phone' => ['string', 'nullable'],
            'birth_date' => ['date', 'nullable'],
            'country' => ['string', 'nullable'],
            'city' => ['string', 'nullable'],
            'street' => ['string', 'nullable'],
        ];
    }
}
