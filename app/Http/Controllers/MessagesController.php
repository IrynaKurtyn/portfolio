<?php

namespace App\Http\Controllers;

use App\Events\NewMessage;
use App\Http\Requests\NewMessageRequest;
use App\Http\Resources\Collections\ContactCollection;
use App\Http\Resources\Collections\MessageCollection;
use App\Http\Resources\Collections\UserCollection;
use App\Models\Message;
use App\Repositories\Interfaces\MessagesRepositoryInterface;


class MessagesController extends Controller
{
    //

    private $repository;

    /**
     * MessagesController constructor.
     * @param MessagesRepositoryInterface $repository
     */
    public function __construct(MessagesRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return ContactCollection
     */
    public function contacts(){

        return new ContactCollection($this->repository->contacts());
    }

    /**
     * @param $id
     * @return MessageCollection
     */
    public function conversation($id = null){

        $this->repository->getUnreadForContact($id)->update(['read' => true]);

        return new MessageCollection($this->repository->messageHistory($id));

    }

    /**
     * @param NewMessageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(NewMessageRequest $request){

        $message = Message::create([
            'from_id' => auth('api')->id(),
            'to_id' => $request->get('contact_id'),
            'text' => $request->get('text'),
        ]);

        broadcast(new NewMessage($message));

        return response()->json($message);

    }

    /**
     * @return mixed
     */
    public function allUnread(){
        $unread = $this->repository->allUnread();

        return new MessageCollection($unread);
    }


}
