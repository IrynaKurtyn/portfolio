<?php

namespace App\Http\Controllers;

use App\Helpers\LiqPay;
use App\Helpers\XMLPaymentGenerator;
use App\Http\Requests\StripeCheckoutRequest;
use Cartalyst\Stripe\Exception\CardErrorException;
use Illuminate\Http\Request;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PaymentController extends Controller
{
    private $xmlGenerator;

    public function __construct(XMLPaymentGenerator $xmlGenerator){
        $this->xmlGenerator = $xmlGenerator;
    }
    //
    public function paymentProcess(StripeCheckoutRequest $request)
    {
        try {
            $token = $request->get('stripeToken');
            $stripe = Stripe::setApiKey(env('STRIPE_SECRET'));
            $customer = $stripe->customers()->create([
                'email' => $request->get('email'),
            ]);

            $response = $stripe->charges()->create([
                'amount' => 0.50,
                'currency' => 'USD',
                'source' => $token['id'],
                'description' => 'Description goes here',
                'receipt_email' => 'kurtyn87@gmail.com',

                'metadata' => [
                    'data1' => 'metadata 1',
                    'data2' => 'metadata 2',
                    'data3' => 'metadata 3'
                ]
            ]);

            return response()->json(['receipt' => $response['receipt_url']]);
        } catch (CardErrorException $e) {

            return response()->json($e->getMessage());
        }
    }


    public function paymentPrivat(){

        $xml = $this->xmlGenerator->initRequest();
        $stream_options = array (
            'http' => array (
                'method' => "POST",
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'content' => $xml
            )
        );
        $context=stream_context_create($stream_options);
        $response=file_get_contents(env('PRIVAT_API_URL'), false, $context);

        $array_data = json_decode(json_encode(simplexml_load_string($response)), true);
        $responseSignature = $array_data['merchant']['signature'];
        if($this->xmlGenerator->checkSignature($response, $responseSignature)){
            dd($array_data);
        }
    }


    //hand made function
    public function getLiqPayDataAndSignatureOwn()
    {
        $data = base64_encode(json_encode([
            'public_key' => env('TEST_LIQ_PAY_PUBLIC_KEY'),
            'version' => '3',
            'action' => 'pay',
            'amount' => '1',
            'currency' => 'UAH',
            'description' => 'Test',
            'order_id' => Str::random(),
            'server_url' => 'http://localhost:8085/api/liqpay-callback'
        ]));


        return response()->json([
            'data' => $data,
            'signature' => base64_encode(sha1(env('TEST_LIQ_PAY_PRIVATE_KEY') . $data . env('TEST_LIQ_PAY_PRIVATE_KEY'), 1))
        ]);
    }


    //hand made function
    public function liqpayResponseOwn(Request $request){

        $responseData = $request->get('data');
        $responseSignature = $request->get('signature');

        $serverSignature = base64_encode(sha1(env('TEST_LIQ_PAY_PRIVATE_KEY') . $responseData . env('TEST_LIQ_PAY_PRIVATE_KEY'), 1));

        if($serverSignature == $responseSignature){
            //payment successfully made
        }
    }


    //hand made function
    public function getLiqPayDataAndSignature(Request $request)
    {
        $params = [
            'public_key' => env('TEST_LIQ_PAY_PUBLIC_KEY'),
            'version' => '3',
            'language' => 'en',
            'action' => 'pay',
            'amount' => $request->get('amount'),
            'currency' => $request->get('currency'),
            'description' => 'Test',
            'order_id' => $request->get('order_id'),
            'server_url' => 'http://localhost:8085/api/liqpay-callback'
        ];

        $liqPay = new LiqPay(env('TEST_LIQ_PAY_PUBLIC_KEY'), env('TEST_LIQ_PAY_PRIVATE_KEY'));

        return response()->json([
            'data' => $liqPay->cnb_form($params),
        ]);
    }


    //hand made function
    public function liqpayResponse(Request $request){

        $responseData = $request->get('data');
        $responseSignature = $request->get('signature');

        $serverSignature = base64_encode(sha1(env('TEST_LIQ_PAY_PRIVATE_KEY') . $responseData . env('TEST_LIQ_PAY_PRIVATE_KEY'), 1));

        if($serverSignature == $responseSignature){
            //payment successfully made
        }
    }

}
