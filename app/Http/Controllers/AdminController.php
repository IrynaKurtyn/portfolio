<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    //

    public function checkPassword(Request $request){

           return response()->json($request->get('password') == env('ADMIN_PASSWORD'));
    }

}
