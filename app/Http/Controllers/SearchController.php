<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Searchable\ModelSearchAspect;
use Spatie\Searchable\Search;

class SearchController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('search');
    }


    public function search( Request $request)
    {
        $searchterm = $request->input('query');
        $searchResults = (new Search())
            ->registerModel(\App\Models\User::class, function(ModelSearchAspect $modelSearchAspect) {
                $modelSearchAspect
                    ->addSearchableAttribute('first_name')
                    ->addSearchableAttribute('last_name')
                    ->addSearchableAttribute('email');
                    })
            ->registerModel(\App\Models\Message::class, function(ModelSearchAspect $modelSearchAspect) {
                $modelSearchAspect
                    ->addSearchableAttribute('text');
            })
            ->registerModel(\App\Models\Event::class, function(ModelSearchAspect $modelSearchAspect) {
                $modelSearchAspect
                    ->addSearchableAttribute('title')
                    ->addSearchableAttribute('description');
            })
            ->perform($searchterm);
        $result = [];

        foreach($searchResults->groupByType() as $type => $modelResults){
            $result[$type] = $modelResults;

        }

        $response = [
            'count' => $searchResults->count(),
            'term' => $searchterm,
            'result' => $result
        ];

        return response()->json($response);
    }

}
