<?php

namespace App\Http\Controllers;

use App\Exports\ExportUsers;
use App\Http\Filters\UserFilter;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Resources\Items\ProfileResource;
use App\Http\Resources\Collections\UserCollection;
use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Twilio\Rest\Client;

class UsersController extends Controller
{

    private $repository;

    /**
     * UsersController constructor.
     * @param UserRepositoryInterface $repository
     */
    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return UserCollection
     */
    public function index(UserFilter $filter)
    {
        $users = $this->repository->all($filter);

        return new UserCollection($users);
    }



    /**
     * @return ProfileResource
     */
    public function profile(){

        return new ProfileResource(auth('api')->user());
    }

    /**
     * @return ProfileResource
     */
    public function updateProfile(UpdateProfileRequest $request){
        $user = auth('api')->user();
        $profile = $user->profile;
        if ($img = $request->file('avatar')) {
            $profile->avatar = $user->profile->saveFile($img);
        }
        $user->update($request->only('first_name', 'email', 'last_name'));
        $profile->fill($request->only( 'phone', 'country', 'city', 'street', 'about', 'birth_date'));
        $profile->save();

       return new ProfileResource($user);
    }


    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $model = User::findOrFail($id);
        return response()->json(new ProfileResource($model), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function update(UpdateProfileRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $profile = $user->profile;
        $user->update($request->only('first_name', 'email', 'last_name'));
        $profile->fill($request->only( 'phone', 'country', 'city', 'street', 'about', 'birth_date'));
        $profile->save();

        return new ProfileResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ids = explode(",", $id);
        User::find($ids)->each(function ($item, $key) {
            if (Auth::user()->id != $item->id) {
                $item->delete();
            } else {
                return false;
            }
        });
        return response()->json('Deleted successfully', 200);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function export(UserFilter $filter)
    {
        return Excel::download(new ExportUsers($filter), 'users.csv');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function chartData(Request $request){
        $data = $this->repository->getChartData($request);
        return response()->json(['data' => $data]);
    }

}
