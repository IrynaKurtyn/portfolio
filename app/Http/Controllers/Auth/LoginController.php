<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Resources\Items\UserResource;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Laravel\Passport\Client;
use Laravel\Passport\Passport;

class LoginController extends Controller
{
    public $successStatus = 200;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */



    /**
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginOldWithoutRefresh(LoginRequest $request)
    {
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expired_at' => $tokenResult->token->expires_at,
            'user' => new UserResource($user)
        ]);
    }


    /**
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        if($request->has('remember_me') && $request->get('remember_me')){
            Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));
        }
        $user = User::where('email', $request['email'])->first();

        $passwordGrantClient = Client::where('password_client', 1)->first();

        $data = [
            'grant_type' => 'password',
            'client_id' => $passwordGrantClient->id,
            'client_secret' => $passwordGrantClient->secret,
            'username' => $request['email'],
            'password' => $request['password'],
            'scope' => '*'
        ];

        $tokenRequest = Request::create('/oauth/token', 'post', $data);

        $tokenResponse = app()->handle($tokenRequest);

        $content = json_decode($tokenResponse->content());
        return response()->json([
            'access_token' => $content->access_token,
            'refresh_token' => $content->refresh_token,
            'token_type' => 'Bearer',
            'expired_at' => date('Y-m-d H:i:s',time() + $content->expires_in),
            'user' => new UserResource($user)
        ]);
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(){
        if (Auth::check()) {
            if(Auth::user()->token()->revoke()){
                return response()->json(['success' => 'logout_success'],200);
            }

        }else{
            return response()->json(['error' => 'Logout can not be resolved'], 500);
        }
    }
}
