<?php
namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Http\Resources\Items\ProfileResource;
use App\Http\Requests\{ForgetPassword,ResetRequest};
use App\Http\Resources\Items\UserResource;
use App\Notifications\ResetPassword;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\{Facades\DB, Facades\Hash, Facades\Log, Str};

class PasswordResetController extends Controller
{
    /**
     * Create token password reset
     * @param ForgetPassword $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forget(ForgetPassword $request)
    {
        $user = User::whereEmail($request->email)->first();
        $token = Str::random(60);
        $passwordReset =  DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);
        if ($user && $passwordReset)
            $user->notify(
                new ResetPassword($token, $request->email)
            );

        return response()->json([
            'message' => 'We sent reset link!'
        ]);
    }


    /**
     * @param ResetRequest $request
     * @return UserResource|\Illuminate\Http\JsonResponse
     */
    public function reset(ResetRequest $request)
    {
        $request->validated();
        $passwordReset = DB::table('password_resets')->where([
            ['token', $request->token],
            ['email', $request->email]
        ])->orderBy('created_at', 'desc')->first();

        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        $user = User::where('email', $passwordReset->email)->firstOrFail();
        $user->password = Hash::make($request->password);
        $user->save();
        DB::table('password_resets')->where('email', $request->email)->delete();
        return new ProfileResource($user);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkToken(Request $request){
        $passwordReset = DB::table('password_resets')->where(
            'token', $request->get('token')
        )->first();

        if(!$passwordReset){
            return response()->json(['message' => 'Token invalid']);
        }

        return response()->json('OK');
    }
}
