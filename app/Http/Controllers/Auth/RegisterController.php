<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\RegisterUserRequest;
use App\Http\Resources\Items\ProfileResource;
use App\Jobs\UserAfterCreateJob;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * @param RegisterUserRequest $request
     */
    public function register(RegisterUserRequest $request)
    {

        $inputs = $request->validated();

        event(new Registered($user = $this->create($inputs)));
        if ($user) {
            $this->dispatch(new UserAfterCreateJob($user));
            $token = $user->createToken('Personal Access Token');
            return response()->json([
                'access_token' => $token->accessToken,
                'user' => new ProfileResource($user),
                'token_type' => 'Bearer',
                'expired_at' => $token->token->expires_at,
            ]);
        }
        else {
            throw new AuthenticationException('Unable to register user.');
        }

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {

        if(!$data['password']){
            $data['password'] = Str::random(6);
        }

        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
