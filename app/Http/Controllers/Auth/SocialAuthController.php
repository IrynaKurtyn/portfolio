<?php


namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Http\Resources\Items\UserResource;
use App\Models\SocialAccount;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        $url = Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();

        return response()->json([
            'url' => $url
        ]);
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {


        $user = Socialite::driver($provider)->stateless()->user();

        if(!$user->token){
            return response()->json([
                "success" => false,
                "message" => "Failed to login"
            ], 401);
        }

        $appUser = User::where('email', $user->email)->first();
        if(!$appUser){
            //create user and add provider
            $firstName = isset(explode(' ', $user->name)[0]) && explode(' ', $user->name)[0] ? explode(' ', $user->name)[0] : 'Not defined';
            $lastName = isset(explode(' ', $user->name)[1]) && explode(' ', $user->name)[1] ? explode(' ', $user->name)[1] : 'Not defined';
            $appUser = User::create([
                'first_name' => $firstName ,
                'last_name' => $lastName,
                'email' => $user->email,
                'password' => Hash::make(Str::random(8)),
            ]);
            SocialAccount::create([
                'provider' => $provider,
                'provider_user_id' => $user->id,
                'user_id' => $appUser->id

            ]);
        } else {
            $socialAccount = $appUser->socialAccounts()->where('provider', $provider)->first();

            //create socialAccount

            if(!$socialAccount){
                 SocialAccount::create([
                    'provider' => $provider,
                    'provider_user_id' => $user->id,
                    'user_id' => $appUser->id

                ]);
            }


        }
        $passportToken = $appUser->createToken('Personal Access Token');

        return response()->json([
            'access_token' => $passportToken->accessToken,
            'token_type' => 'Bearer',
            'expired_at' => $passportToken->token->expires_at,
            'user' => new UserResource($appUser)
        ]);

    }

}
