<?php

namespace App\Http\Controllers;

use App\Http\Filters\EventFilter;
use App\Http\Resources\Collections\EventCollection;
use App\Http\Resources\Items\EventResource;
use App\Models\Event;
use App\Http\Requests\NewEventRequest;
use App\Repositories\Interfaces\EventsRepositoryInterface;
use App\Repositories\Interfaces\MessagesRepositoryInterface;
use Carbon\Carbon;

class EventsController extends Controller
{

    private $repository;

    /**
     * MessagesController constructor.
     * @param MessagesRepositoryInterface $repository
     */
    public function __construct(EventsRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return EventCollection
     */
    public function index(EventFilter $filter)
    {
//        Event::truncate();
        return new EventCollection($this->repository->all($filter));
    }

    /**
     * @param NewEventRequest $request
     * @return EventResource
     */
    public function store(NewEventRequest $request)
    {
        $event = new Event();

       return $event->createFromRequest($request);

    }

    /**
     * @param Event $event
     * @return EventResource
     */
    public function show(Event $event)
    {
        return new EventResource($event);
    }

    /**
     * @param NewEventRequest $request
     * @param Event $event
     * @return EventResource
     */
    public function update(NewEventRequest $request, Event $event)
    {
        $event->update($request->except('backGroundColor'));
        return new EventResource($event);
    }

    /**
     * @param Event $event
     */
    public function destroy(Event $event)
    {
        if($event->delete()){
            return response()->json(null, 204);
        }
    }
}
