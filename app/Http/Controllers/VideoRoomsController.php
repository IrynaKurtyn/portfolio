<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Twilio\Rest\Client;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;

class VideoRoomsController extends Controller
{
    //

    public function __construct()
    {
        $this->sid = config('services.twilio.sid');
        $this->token = config('services.twilio.token');
        $this->key = config('services.twilio.key');
        $this->secret = config('services.twilio.secret');
    }


    public function getAccessToken(){
        $identity = Auth::user()->getName(). '('. Auth::id() . ')';
        Log::debug("joined with identity: $identity");
        $token = new AccessToken($this->sid, $this->key, $this->secret, 3600, $identity);
        $grant = new VideoGrant();
        $grant->setRoom('Ira room');
        $token->addGrant($grant);

        // Serialize the token as a JWT

      return response()->json(['accessToken' => $token->toJWT()]);
    }

    public function index()
    {
        $rooms = [];
        try {
            $client = new Client($this->sid, $this->token);
            $allRooms = $client->video->rooms->read([]);

            $rooms = array_map(function($room) {
                return $room->uniqueName;
            }, $allRooms);

        } catch (\Exception $e) {
            echo "Error: " . $e->getMessage();
        }
        return response()->json(['rooms' => $rooms]);
    }

    public function createRoom(Request $request)
    {

        $client = new Client($this->sid, $this->token);

        $exists = $client->video->rooms->read([ 'uniqueName' => $request->roomName]);

        if (empty($exists)) {
            $client->video->rooms->create([
                'uniqueName' => $request->roomName,
                'type' => 'group',
                'recordParticipantsOnConnect' => false
            ]);
            Log::debug("created new room: ".$request->roomName);

            return response()->json(['roomName' => $request->roomName]);
        } else {
            return response()->json(['error' => 'Name is olready in use'], 422);
        }


    }
}
