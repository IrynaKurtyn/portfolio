<?php

namespace App\Exports;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportUsers implements FromCollection, WithHeadings
{
    /**
     * @var
     */
    public $filter;

    /**
     * ExportClients constructor.
     * @param $filter
     */
    public function __construct($filter){
        $this->filter = $filter;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $query = User::filter($this->filter);

        return $query
            ->select('first_name', 'last_name', 'email')
            ->with(array('profile'=>function($query){
                $query->select('phone', 'country', 'city', 'about', 'created_at');
            }))
            ->get();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return ['First Name', 'Last Name', 'Email', 'Phone', 'Country', 'City', 'About', 'Created at'];
    }
}
