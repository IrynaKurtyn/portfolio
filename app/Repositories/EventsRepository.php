<?php


namespace App\Repositories;


use App\Models\Event;
use App\Repositories\Interfaces\EventsRepositoryInterface;

class EventsRepository implements EventsRepositoryInterface
{
    /**
     * @return mixed
     */
    public function all($filter){
        return Event::filter($filter)->where('user_id', auth()->id())->get();
    }
}
