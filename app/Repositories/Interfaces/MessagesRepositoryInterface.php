<?php


namespace App\Repositories\Interfaces;


interface MessagesRepositoryInterface
{
    public function contacts();

    public function messageHistory($id);

    public function getUnreadForContact($id);

    public function allUnread();
}