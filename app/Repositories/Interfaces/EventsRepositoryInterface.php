<?php


namespace App\Repositories\Interfaces;


interface EventsRepositoryInterface
{

    public function all($filter);

}
