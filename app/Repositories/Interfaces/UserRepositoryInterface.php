<?php


namespace App\Repositories\Interfaces;


interface UserRepositoryInterface
{
    public function all($filter);

    public function getChartData($request);

}
