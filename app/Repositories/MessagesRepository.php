<?php


namespace App\Repositories;


use App\Models\Message;
use App\Models\User;
use App\Repositories\Interfaces\MessagesRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class MessagesRepository implements MessagesRepositoryInterface
{

    /**
     * @return mixed
     */
    public function contacts(){
        $contacts = User::with('profile')->get()->except(Auth::id());

        $unreadIds = Message::select(\DB::raw('`from_id` as sender_id, count(`from_id`) as messages_count'))
            ->where('to_id', auth()->id())
            ->where('read', false)
            ->groupBy('from_id')
            ->get();

        $contacts = $contacts->map(function($contact) use ($unreadIds) {
            $contactUnread = $unreadIds->where('sender_id', $contact->id)->first();

            $contact->unread = $contactUnread ? $contactUnread->messages_count : 0;

            return $contact;
        });

        return $contacts;
    }

    /**
     *
     */
    public function messageHistory($id){

        return Message::where(function($q) use ($id){
           $q->where('from_id', auth()->id());
           $q->where('to_id', $id);
        })->orWhere(function($q) use ($id){
            $q->where('from_id', $id);
            $q->where('to_id', auth()->id());
        })->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getUnreadForContact($id){
        $query = Message::query();
        if($id){
            $query->where('from_id', $id);
        }
        return $query->where('to_id', auth()->id());

    }

    /**
     * @return mixed
     */
    public function allUnread(){
        return Message::where('to_id', auth()->id())
            ->where('read', false)->get();
    }
}
