<?php


namespace App\Repositories;


use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Carbon\Carbon;

class UserRepository implements UserRepositoryInterface
{
    const LAST_WEEK = 1;
    const LAST_3_MONTHS = 2;
    const LAST_YEAR = 3;

    /**
     * @return mixed
     */
    public function all($filter){
        return User::with('profile')
            ->filter($filter)
            ->paginate($filter->request->get('per_page', 10));
    }

    /**
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getChartData($request){

        $period = $request->get('period', self::LAST_3_MONTHS);
        $query = User::query();
        switch ($period){
            case 1:
                $query->whereBetween('created_at', [Carbon::today()->subWeek(), Carbon::now()]);
                break;
            case 2:
                $query->whereBetween('created_at', [Carbon::today()->subMonths(3), Carbon::now()]);
                break;
            case 3:
                $query->whereBetween('created_at', [Carbon::today()->subYear(), Carbon::now()]);
                break;

        }
       $chartData = $query->get()->groupBy(function($date) use ($period) {
           switch ($period){
               case self::LAST_WEEK :
                   return $date->created_at->format('D');
                   break;
               case self::LAST_3_MONTHS :
                   return $date->created_at->format('W Y');
                   break;
               case self::LAST_YEAR:
                   return $date->created_at->format('M Y');
                   break;

           }
       });

        $res = [];

        switch ($period){
            case self::LAST_WEEK :
                for( $i = Carbon::today()->subWeek(); $i <= Carbon::now(); $i->addDay() ){
                    if(!isset($chartData[$i->format('D')])){
                        $res[$i->format('D')] = [];
                    } else {
                        $res[$i->format('D')] = $chartData[$i->format('D')];
                    }
                }
                break;
            case self::LAST_3_MONTHS :
                for( $i = Carbon::today()->subMonths(3); $i <= Carbon::now(); $i->addWeek() ){
                    if(!isset($chartData[$i->format('W Y')])){
                        $res[$i->format('W Y')] = [];
                    } else {
                        $res[$i->format('W Y')] = $chartData[$i->format('W Y')];
                    }
                }
                break;
            case self::LAST_YEAR:
                for( $i = Carbon::today()->subYear(); $i <= Carbon::now(); $i->addMonth() ){
                    if(!isset($chartData[$i->format('M Y')])){
                        $res[$i->format('M Y')] = [];
                    } else {
                        $res[$i->format('M Y')] = $chartData[$i->format('M Y')];
                    }
                }
                break;

        }

        $results = collect($res)->transform(function($item, $key) {
                return $item = count($item);
        })->keyBy(function($item, $key) use ($period) {
                if($period == 2){
                    return $this->getStartAndEndDate($key)['week_start'];
                } else {
                    return $key;
                }
        });

        $newArray = $datasets = [];
        $newArray['labels'] = [];

        $datasets['label'] = 'Count of registered users';
        $datasets['data'] = [];
        $datasets['backgroundColor'] = '#e69809';

        foreach ($results  as $key => $element){
            array_push($newArray['labels'], $key);
            array_push($datasets['data'], $element);
        }

        $newArray['datasets'][] = $datasets;

        return $newArray;
    }


    /**
     * @param $week
     * @param int $year
     * @return mixed
     * @throws \Exception
     */
    private function getStartAndEndDate($key) {
        $week = explode(' ', $key)[0];
        $year = explode(' ', $key)[1];
        $dto = new Carbon();
        $dto->setISODate($year, $week);
        $result['week_start'] = $dto->format('Y-m-d');
        $dto->modify('+6 days');
        $result['week_end'] = $dto->format('Y-m-d');
        return $result;
    }
}
