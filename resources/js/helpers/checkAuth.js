import store from '../store';

export default function checkAuth(){
  let token = localStorage.getItem('token');
  if(!store.isAuthorized && (token !== null) && token.length) {
    store.dispatch('setAuthStorage')
  }
}