const Swal = require('sweetalert2')
import 'sweetalert2/src/sweetalert2.scss'
import '../sass/customSwal.scss'
window.Swal = Swal
window.sweetToast = ((status, text) => {
    let options = {
        width: 261,
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        text: text,
        type: status,
    }

    Swal.fire(options)
})

window.successToast = ((text = null)=>{
    sweetToast('success', text ? text :'Successfully done!')});

window.errorToast = ((e,text = null)=>{
    console.log(e)
    if (!text){
        switch (e.response.status) {
            case 422: {
                text = 'Not valid data';
                break;
            }
            case 404:{
                text = 'Page not found';
                break;
            }
            case 401:{
                text = 'Unauthorized';
                break;
            }
            default:{
                text = 'Server error (Response '+e.response.status+')'
                break;
            }
        }
    }
    sweetToast('error', text)
});


let alertOptions = {
    padding: '0',
    confirmButtonText: "OK",
    customClass: {
        container: 'container-class',
        popup: 'popup-class',
        header: 'header-class',
        title: 'title-class',
        closeButton: 'close-button-class',
        icon: 'icon-class',
        image: 'image-class',
        content: 'content-class',
        input: 'input-class',
        actions: 'actions-class',
        confirmButton: 'confirm-button-class',
        cancelButton: 'cancel-button-class',
        footer: 'footer-class'
    },
    confirmButtonColor : "#a087cc",
    cancelButtonColor : "#cf884e",
}

window.sweetAlert = ((title, text) => {
        alertOptions.text = text
        alertOptions.title = title
    Swal.fire(alertOptions)
})

window.sweetDelete = ((text) => {
    let deleteOptions =
    {
        ...alertOptions,
        title: "Confirmation",
        text : 'Are you sure?',
        showCancelButton : true,
        showConfirmButton : true,
        confirmButtonText: "Yes",
        cancelButtonText: "Cancel",
    }
   return Swal.fire(deleteOptions)
});








