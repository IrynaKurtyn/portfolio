export default {
  methods: {
    validate(){
      if(this.$v.$invalid){
          console.log('inv')
        this.$v.$touch()
        return
      }
    },
    showServerErrors(e){
      if (e.response.status == 422){
        this.validationErrors = e.response.data.errors;
      }
    }
  },
}
