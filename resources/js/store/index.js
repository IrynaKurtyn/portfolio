import Vue from 'vue';
import Vuex from 'vuex';
import auth from './auth';
import chat from './chat';
import events from './events';
import charts from './charts';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        error: null,
        profile: {
            id: null,
            first_name: null,
            last_name: null,
            birth_date: null,
            email: null,
            street: null,
            country: null,
            city: '',
            about: null,
            avatar: null,
            avatar_path: null,
        },
    },
    mutations: {
        setError(state, error) {
            state.error = error;
        },
        clearError(state) {
            state.error = null;
        },
        setProfile(state, payload) {
            Object.assign(state.profile, payload);
        },
    },
    getters: {
        error: s => s.error,
        getProfile: s => s.profile,
    },
    actions: {
        getProfile(context) {
            axios.get('/api/profile').then((response) => {
                context.commit('setProfile', response.data.data);
            });
        },
        updateProfile(context, info) {
            return new Promise((resolve, reject) => {
                axios.post('/api/profile', info).then((response) => {
                    console.log(response)
                    context.commit('setProfile', response.data.data);
                    context.commit('setUser', response.data.data);
                    localStorage.setItem('user', JSON.stringify(response.data.data));
                    resolve();
                }).catch((e) => {
                    reject(e);
                });
            });

        },


    },
    modules: {
        auth,
        chat,
        events,
        charts
    },
});
