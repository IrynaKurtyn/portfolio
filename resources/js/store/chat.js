
export default {

  state: {
    contacts: [],
    messages: [],
    unread: [],
  },

  getters:{
    getContacts: s => s.contacts,
    getMessages: s => s.messages,
    getUnread: s => s.unread,
  },

  mutations: {
    setContacts(state, payload) {
      state.contacts = payload
    },
    setMessages(state, payload){
      state.messages = payload;
    },
    setUnread(state, payload){
      state.unread = payload;
    },

  },

  actions: {
    getContacts(context){
      axios.get('/api/contacts').then((response) => {
        context.commit('setContacts', response.data.data);
      });
    },
    getConversation(context, id){
      axios.get('/api/conversation/' + id).then(response => {
        context.commit('setMessages', response.data.data)
      })
    },
    getUnread(context){
      axios.get('/api/all-unread').then(response => {
        context.commit('setUnread', response.data.data)
      })
    }

  }
}