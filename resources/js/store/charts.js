
export default {
    state: {
        usersData: {
            labels: null,
            datasets: [

            ]
        },
    },
    getters:{
        getUsersChartData: s => s.usersData,

    },
    mutations: {
        setUserData(state, payload) {
            Object.assign(state.usersData, payload)
        },
    },
    actions: {
        getUserData(context){
            axios.get('/api/users-charts-data').then((response) => {
                context.commit('setUserData', response.data.data);
            });
        },

    }
}
