import router from '../router'
import moment from 'moment/moment';
export default {

  state: {
    user: JSON.parse(localStorage.getItem('user')) || {},
    authorized: false,
    token: localStorage.getItem('token') || '',
  },

  getters:{
    getUser: state => state.user,
    isAuthorized: state => state.authorized,
  },

  mutations: {
    setUser: (state, payload) => {
      state.user = payload
    },
    setToken: (state, payload) => {
      state.token = payload
    },
    setAuthorized: (state, payload) => {
      state.authorized = payload
    },
  },

  actions: {
    async login({dispatch, commit}, {email, password, remember_me, re_captcha}) {
      try {
        await axios.post('/api/login', {email: email, password: password, remember_me: remember_me, re_captcha: re_captcha}).then(response => {
          dispatch('setAuth', response)
        })
      } catch (e) {
        throw e
      }
    },

    async loginSocial({}, payload) {
      const provider = payload.provider
      try {
        await axios.get('/api/authorize/' + provider + '/redirect').then(response => {
          if(response.data.url){
            window.location.href = response.data.url
          }
        })
      } catch (e) {
        throw e
      }
    },
    async loginSocialCallback({dispatch}, payload) {

      try {
       await axios.get('/api/authorize/' + payload.provider + '/callback', {params: payload}).then(response => {
          dispatch('setAuth', response)
          router.push({name: 'Home'});
        })
      } catch (e) {
        throw e
      }
    },

    async register ({dispatch, commit}, registerData){
      try {
        await axios.post('/api/register', registerData).then(response => {

          dispatch('setAuth', response)
        })
      } catch (e) {
        commit('setError', e)
        throw e
      }
    },

    async logout({dispatch}) {
      await axios.post('/api/logout').then(response => {
       dispatch('clearAuth', response)
      });
    },
    async forget({}, {email}) {
      await axios.post('/api/password/forget', {email}).then(response => {
      });
    },
    async reset({}, data) {
      await axios.post('/api/password/reset', data).then(response => {
      });
    },

    clearAuth({commit, dispatch}, response){
        commit('setToken', null);
        commit('setUser', null);
        commit('setAuthorized', false);
        localStorage.removeItem('user');
        localStorage.removeItem('token');
        localStorage.removeItem('expired_at');

    },

    setAuth({commit, dispatch}, response){
      axios.defaults.headers.common['Authorization'] = `Bearer ${ response.data.access_token }`;
      localStorage.setItem('user', JSON.stringify(response.data.user));
      localStorage.setItem('token', response.data.access_token);
      localStorage.setItem('expired_at', response.data.expired_at);
      commit('setAuthorized', true);
      commit('setToken', response.data.access_token);
      commit('setUser', response.data.user);

    },

    setAuthStorage({commit}){
      commit('setAuthorized', true);
      commit('setToken',localStorage.getItem('token'));
      commit('setUser', JSON.parse(localStorage.getItem('user')));
      axios.defaults.headers.common['Authorization'] = `Bearer ${ localStorage.getItem('token') }`;
    },

    initAuthCheck({commit, dispatch}){
      axios.defaults.headers.common['Authorization'] = `Bearer ${ localStorage.getItem('token') }`;
      const expired_at =  localStorage.getItem('expired_at');

        if((expired_at !== null && expired_at !== undefined) && Date.now() < moment(expired_at).format("x")){
          commit('setAuthorized', true);
        } else if( Date.now() > moment(expired_at).format("x")) {
          console.log('Token expired')
          dispatch("clearAuth")
          return router.push('/login')
        }


      axios.interceptors.response.use(undefined, function (err) {
        return new Promise(function (resolve, reject) {
          if (err.response.status === 401) {
            dispatch("clearAuth")
            router.push('/login')
          }
          throw err;
        });
      });
    },
      //
      // getAuthUser(store){
      //   axios.get('/api/user').then(resp => {
      //       store.commit('setUser', resp.data)
      //       localStorage.setItem('user', JSON.stringify(resp.data));
      //   })
      // }

  }
}
