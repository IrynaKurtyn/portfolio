export default {

    state: {
        events: [],
        nextWeekEvents: []
    },

    getters:{
        getEvents: s => s.events,
        getNextWeekEvents: s => s.nextWeekEvents,
    },

    mutations: {
        setEvents(state, payload) {
            state.events = payload;
        },
        addEvent(state, event) {
            state.events.push(event);
        },
        setNextWeekEvents(state, payload) {
            state.nextWeekEvents = payload;
        },

    },

    actions: {
        getEvents(context) {
            axios.get('/api/events').then((response) => {
                context.commit('setEvents', response.data.data);
            }).catch();
        },
        addEvent(context, info) {
            return new Promise((resolve, reject) => {
                axios.post('/api/events', info).then((response) => {
                    if (Array.isArray(response.data.data)) {
                        context.dispatch('getEvents');
                    } else {
                        context.commit('addEvent', response.data.data);
                    }
                    resolve();
                }).catch((e) => {
                    reject(e);
                });
            });
        },
        updateEvent(context, info) {
            return new Promise((resolve, reject) => {
                axios.put('/api/events/' + info.id, info).then((response) => {
                    context.dispatch('getEvents');
                    resolve();
                }).catch((e) => {
                    reject(e);
                });
            });
        },

        getNextWeekEvents(context) {
            axios.get('/api/events?nextweek=1').then((response) => {
                context.commit('setNextWeekEvents', response.data.data);
            }).catch((e) => {
            });
        },

    }
}
