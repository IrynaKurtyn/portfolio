import moment from 'moment';
export default function timePass(value){
return moment(value).startOf('hour').fromNow()
}