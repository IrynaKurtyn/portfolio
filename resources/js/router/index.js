import Vue from 'vue';
import VueRouter from 'vue-router';
import NotFound from '../components/NotFound';

import auth from './auth';
import Profile from '../pages/Profile';
import Chat from '../pages/Chat';

Vue.use(VueRouter);

const routes = [
    ...auth,
    {
        path: '/',
        name: 'Home',
        meta: {
            layout: 'main',
            requiresAuth: true,
        },
        component: () => import('../pages/Users'),
    },

    {
        path: '/profile',
        name: 'Profile',
        meta: {
            layout: 'main',
            requiresAuth: true,
        },
        component: () => import('../pages/Profile'),
    },

    {
        path: '/chat',
        name: 'Chat',
        meta: {
            layout: 'main',
            requiresAuth: true,
        }, /**/
        component: () => import('../pages/Chat'),
        props: true,
    },
    {
        path: '/charts',
        name: 'Charts',
        meta: {
            layout: 'main',
            requiresAuth: true,
        }, /**/
        component: () => import('../pages/Charts'),
        props: true,
    },
    {
        path: '/calendar',
        name: 'Calendar',
        meta: {
            layout: 'main',
            requiresAuth: true,
        },
        component: () => import('../pages/Calendar'),
    },
    {
        path: '/stripe',
        name: 'Stripe',
        meta: {
            layout: 'main',
            requiresAuth: true,
        },
        component: () => import('../pages/Stripe'),
    },
    {
        path: '/liqpay',
        name: 'LiqPay',
        meta: {
            layout: 'main',
            requiresAuth: true,
        },
        component: () => import('../pages/LiqPay'),
    },
    {
        path: '/video',
        name: 'Video',
        meta: {
            layout: 'main',
            requiresAuth: true,
        },
        component: () => import('../pages/VideoChat'),
    },
    {
        path: '*',
        meta: {
            layout: localStorage.getItem('previousLayout') || 'login',
        },
        component: NotFound,
    },

];

var router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

router.beforeEach((to, from, next) => {

    localStorage.setItem('previousLayout', to.meta.layout);

    var token = localStorage.getItem('token');

    if (to.matched.some(record => record.meta.requiresAuth)) {

        if (token === null) {
            next('/login');
        }
    } else if (to.matched.some(record => record.meta.requiresVisitor)) {
        if (token !== null) {
            next({
                path: '/',
            });
        } else {
            next();
        }
    } else {
        next();
    }

    next();
});
export default router;
