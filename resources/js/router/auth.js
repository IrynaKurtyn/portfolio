
import Login from '../pages/Login';
import Register from '../pages/Register';
import ForgotPassword from '../pages/ForgotPassword';
import ResetPassword from '../pages/ResetPassword';
import LoginSocial from '../pages/LoginSocial';

const routes = [{
  path: '/login',
    name: 'Login',
  meta: {
  layout: 'login',
    requiresVisitor: true
},
  component: Login,
},
{
  path: '/authorize/:provider/callback',
    name: 'LoginSocial',
  meta: {
  layout: 'login',
    requiresVisitor: true
},
  component: LoginSocial,
    props:  (route) => ({ provider: route.params.provider })
},
{
  path: '/forgot',
    name: 'Forgot',
  meta: {
  layout: 'login',
    requiresVisitor: true
},
  component: ForgotPassword,
},
{
  path: '/reset',
  name: 'passwordReset',
  component: ResetPassword,
  meta: {
  layout: 'login',
    requiresVisitor: true
},
},
{
  path: '/register',
    name: 'Register',
  meta: {
  layout: 'login',
    requiresVisitor: true
},
  component: Register,
},
]

export default routes;