import ValidationErrors from './components/ValidationErrors';

require('./bootstrap');


require('./src/jquery.dcjqaccordion.2.7');
require('./src/scripts');
require('./src/bootstrap.min');


import Vue from 'vue';
import vueCountryRegionSelect from 'vue-country-region-select'
import Vuelidate from 'vuelidate';
import router from './router';
import store from './store';
import modal from 'vue-js-modal'
import App from '../App.vue'
import ValidateMixin from './mixins/validate.js'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './swal';
Vue.component('validation-errors', ValidationErrors)
Vue.component('Pagination', require('laravel-vue-pagination'));


Vue.use(Vuelidate);
Vue.mixin(ValidateMixin)
Vue.use(ElementUI);
Vue.use(vueCountryRegionSelect);
Vue.use(modal, {dialog: true,dynamic: true})

const app = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');





