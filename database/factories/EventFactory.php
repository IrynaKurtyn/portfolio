<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Event;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'description' => $faker->sentence,
        'start' => $faker->iso8601,
        'end' => $faker->iso8601(),
        'priority' => rand(0,2),
        'all_day' => rand(0,1),
        'user_id' => 1
    ];
});
