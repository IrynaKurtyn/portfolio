<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Message;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Message::class, function (Faker $faker) {
    do {
        $from_id = rand(1,20);
        $to_id = rand(1,20);
    } while($from_id === $to_id);
    return [
        'from_id' => $from_id,
        'to_id' => $to_id,
        'text' => $faker->sentence

    ];
});