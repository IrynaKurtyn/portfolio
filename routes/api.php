<?php

use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Auth'], function () {
    Route::group(['middleware' => 'guest'], function () {
        Route::post('/register', 'RegisterController@register');
        Route::post('/login', ['as' => 'login', 'uses' => 'LoginController@login']);

            // your routes here
            Route::get('/authorize/{provider}/redirect', 'SocialAuthController@redirectToProvider')->name('api.social.redirect');
            Route::get('/authorize/{provider}/callback', 'SocialAuthController@handleProviderCallback')->name('api.social.callback');

        Route::group(['prefix' => 'password'], function () {
            Route::post('forget', 'PasswordResetController@forget');
            Route::post('reset', 'PasswordResetController@reset');
            Route::post('/check-token', 'PasswordResetController@checkToken');
        });
    });
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('/details', 'LoginController@details');
        Route::post('/logout', 'LoginController@logout');
        Route::get('/user', function (Request $request) {
            return $request->user();
        });
    });

});
Route::post('/privat', 'PaymentController@paymentPrivat');
Route::group(['middleware' => 'auth:api'], function () {
Route::resource('/users', 'UsersController')->except(['edit', 'create']);
Route::resource('/events', 'EventsController')->except(['edit', 'create']);
Route::get('/contacts', 'MessagesController@contacts');
Route::get('/profile', 'UsersController@profile');
Route::get('/users-charts-data', 'UsersController@chartData');
Route::post('/profile', 'UsersController@updateProfile');
Route::get('/conversation/{id?}', 'MessagesController@conversation')->name('messages');
Route::get('/all-unread', 'MessagesController@allUnread');
Route::get('/search', 'SearchController@search');
Route::post('/liqpay-data', 'PaymentController@getLiqPayDataAndSignature');
Route::post('/liqpay-callback', 'PaymentController@liqpayResponse');
Route::post('/conversation/send', 'MessagesController@create');
    Route::post('/users/export', 'UsersController@export');
    Route::post('/payment', 'PaymentController@paymentProcess');
    Route::post('/check-admin-password', 'AdminController@checkPassword');

    Route::get('/rooms', "VideoRoomsController@index");
    Route::get('/rooms/access-token', 'VideoRoomsController@getAccessToken');
    Route::prefix('room')->group(function() {

        Route::get('join/{token}/{roomName}', 'VideoRoomsController@joinRoom');
        Route::post('create', 'VideoRoomsController@createRoom');
    });
});

